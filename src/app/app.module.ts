import { BrowserModule } from '@angular/platform-browser';
import {forwardRef, NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SuppliersListComponent } from './suppliers/suppliers-list/suppliers-list.component';
import { TitleHeaderComponent } from './suppliers/suppliers-list/title-header/title-headercomponent';
import {SupplierService} from './suppliers/services/supplier-service/supplier.service';
import { SupplierEditComponent } from './suppliers/suppliers-list/supplier-edit/supplier-edit.component';
import {ReactiveFormsModule} from '@angular/forms';
import {FormDataService} from './suppliers/form/form-services/form-data-service/form-data.service';
import {FormControlService} from './suppliers/form/form-control-service/form-control.service';
import { DynamicFormComponent } from './suppliers/form/dynamic-form/dynamic-form.component';
import { MultiselectDropdownComponent } from './suppliers/form/multiselect-dropdown/multiselect-dropdown.component';
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SuppliersListComponent,
    TitleHeaderComponent,
    SupplierEditComponent,
    DynamicFormComponent,
    MultiselectDropdownComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [SupplierService, FormDataService, FormControlService],
  bootstrap: [AppComponent]
})
export class AppModule { }
