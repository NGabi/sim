import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SupplierEditComponent} from './suppliers/suppliers-list/supplier-edit/supplier-edit.component';
import {SuppliersListComponent} from './suppliers/suppliers-list/suppliers-list.component';


const routes: Routes = [
  { path: '', redirectTo: 'suppliers', pathMatch: 'full' },
  { path: 'suppliers', component: SuppliersListComponent },
  { path: 'suppliers/new', component: SupplierEditComponent },
  { path: 'suppliers/:id', component: SupplierEditComponent },
  // {path: '**', component:ErrorComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
