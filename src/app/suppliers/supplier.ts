export class Supplier {
   ggn: number;
   name: string;
   country: string;
   roles: string[];
   sector: string;

  constructor(ggn: number, name: string, country?: string, roles?: string[], sector?: string) {
    this.ggn = ggn;
    this.name = name;
    this.country = country;
    this.roles = roles;
    this.sector = sector;
  }
}
