import { Injectable } from '@angular/core';
import {Supplier} from '../../supplier';

@Injectable({
  providedIn: 'root'
})
export class SupplierService {
  private suppliers: Supplier[] = [
    new Supplier(24434665, 'Sociedad Santa Luisa', 'Spain', ['Primary Producer', 'Additional Supplier']),
    new Supplier(24434665, 'Wonderful ltd.', 'Spain', ['Primary Producer']),
    new Supplier(24434665, 'Wonderful ltd.', 'Spain', ['Primary Producer', 'Additional Supplier']),
    new Supplier(24434665, 'Wonderful ltd.', 'Spain', ['Primary Producer']),
    new Supplier(24434665, 'Wonderful ltd.', 'Spain', ['Primary Producer']),
    new Supplier(24434665, 'Wonderful ltd.', 'Spain', ['Primary Producer', 'Additional Supplier']),
    new Supplier(24434665, 'Wonderful ltd.', 'Spain', ['Primary Producer'])
  ];
  constructor() { }

  getSuppliers() {
    return this.suppliers.slice();
  }
  getSupplier(id: number){
    return this.suppliers[id];
  }
}
