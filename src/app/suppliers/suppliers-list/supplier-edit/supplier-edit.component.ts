import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {SupplierService} from '../../services/supplier-service/supplier.service';
import {FormFieldModel} from '../../form/form-services/form-models/form-field.model';
import {FormGroup} from '@angular/forms';
import {FormControlService} from '../../form/form-control-service/form-control.service';
import {FormDataService} from '../../form/form-services/form-data-service/form-data.service';

@Component({
  selector: 'app-edit',
  templateUrl: './supplier-edit.component.html',
  styleUrls: ['./supplier-edit.component.scss']
})
export class SupplierEditComponent implements OnInit {
  headerTitle: string;
  id: number;
  fields: FormFieldModel<any>[];
  form: FormGroup;
  payLoad = '';

  constructor(private route: ActivatedRoute,
              private router: Router,
              private supplierService: SupplierService,
              private formService: FormControlService,
              private formDataService: FormDataService) {
  }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.initiateFormFields(params['id']);
        }
      );
  }

  initiateFormFields(id: string) {
    if (id != null) {
      // If an item is being edited
      this.fields = this.formDataService.getFields(+id);
      this.headerTitle = this.supplierService.getSupplier(this.id).name;
    } else {
      // If a new item is being created
      this.fields = this.formDataService.getNewItemFields();
      this.headerTitle = 'New supplier';
    }
    this.form = this.formService.toFormGroup(this.fields);
  }

  onCancel() {
    this.router.navigate(['/suppliers'], {relativeTo: this.route});
  }

  onSubmit() {
    this.payLoad = JSON.stringify(this.form.value);
    console.log(this.payLoad);
    this.router.navigate(['/suppliers'], {relativeTo: this.route});
  }
}
