import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierEditComponent } from './supplier-edit.component';
import {TitleHeaderComponent} from '../title-header/title-headercomponent';
import {ReactiveFormsModule} from '@angular/forms';
import {DynamicFormComponent} from '../../form/dynamic-form/dynamic-form.component';
import {MultiselectDropdownComponent} from '../../form/multiselect-dropdown/multiselect-dropdown.component';
import {RouterTestingModule} from '@angular/router/testing';
import {SupplierService} from '../../services/supplier-service/supplier.service';
import {FormControlService} from '../../form/form-control-service/form-control.service';
import {FormDataService} from '../../form/form-services/form-data-service/form-data.service';

describe('EditComponent', () => {
  let component: SupplierEditComponent;
  let fixture: ComponentFixture<SupplierEditComponent>;
  let mockSupplierService;
  let mockFormService;
  let mockFormDataService;

  beforeEach(async(() => {
    mockSupplierService = jasmine.createSpyObj(['getSuppliers', 'getSupplier']);
    mockFormService = jasmine.createSpyObj(['toFormGroup']);
    mockFormDataService = jasmine.createSpyObj(['getFields', 'getNewItemFields']);
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, RouterTestingModule],
      providers: [ { provide: SupplierService, useValue: mockSupplierService },
        { provide: FormControlService, useValue: mockFormService },
        { provide: FormDataService, useValue: mockFormDataService }],
      declarations: [SupplierEditComponent, TitleHeaderComponent,
        DynamicFormComponent, MultiselectDropdownComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
