import { Component, OnInit } from '@angular/core';
import {Supplier} from '../supplier';
import {SupplierService} from '../services/supplier-service/supplier.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-suppliers-list',
  templateUrl: './suppliers-list.component.html',
  styleUrls: ['./suppliers-list.component.scss']
})
export class SuppliersListComponent implements OnInit {
  suppliers: Supplier[] = [
    new Supplier(24434665, 'Sociedad Santa Luisa', 'Spain',
      ['Primary Producer'], 'Strobery'),
    new Supplier(24434665, 'Wonderful ltd.', 'Spain',
      ['Primary Producer'], 'Strobery')
  ];
  tableColums = ['GGN', 'Name', 'Country', 'Roles', 'Sector'];
  isActive = -1;
  constructor(private supplierService: SupplierService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.suppliers = this.supplierService.getSuppliers();
  }

  onNewSupplier() {
    this.router.navigate(['new'], {relativeTo: this.route});
  }
}
