import {TestBed} from '@angular/core/testing';

import {FormControlService} from './form-control.service';
import {MultiselectDropdownField} from '../form-services/form-models/multiselect-dropdown-field.model';
import {TextField} from '../form-services/form-models/text-form-field.model';
import {DropdownField} from '../form-services/form-models/dropdown-form-field.model';

describe('FormServiceService', () => {
  let FIELDS;
  let service: FormControlService;
  beforeEach(() => {
    service = TestBed.get(FormControlService);
    FIELDS =  [new TextField({
      key: 'name',
      label: 'Name',
      value: 'Wonderful Ltd',
      required: true,
      order: 1
    }),
      new TextField({
        key: 'ggn',
        label: 'GGN',
        value: '',
        order: 2
      }),
      new DropdownField({
        key: 'country',
        label: 'Country',
        options: [
          {key: 'solid',  value: 'Solid'},
          {key: 'great',  value: 'Great'},
          {key: 'good',   value: 'Good'},
          {key: 'unproven', value: 'Unproven'}
        ],
        order: 3
      }),
      new MultiselectDropdownField({
        key: 'roles',
        label: 'Roles',
        value: [{key: 'test1', value: 'Test1'}],
        options: [
          {key: 'test1',  value: 'Test1'},
          {key: 'test2',  value: 'Test2'}
        ],
        order: 4
      }),
      new MultiselectDropdownField({
        key: 'sector',
        label: 'Sector',
        value: [{key: 'test1', value: 'Test1'},
          {key: 'test2', value: 'Test2'}],
        options: [
          {key: 'test1',  value: 'Test1'},
          {key: 'test2',  value: 'Test2'}
        ],
        order: 5
      })];
  });
  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return a form group', () => {
      const payload = {
        'name': 'Wonderful Ltd', 'ggn': '', 'country': '', 'roles': [{'key': 'test1', 'value': 'Test1'}]
        , 'sector': [{'key': 'test1', 'value': 'Test1'}, {'key': 'test2', 'value': 'Test2'}]
      };
      const formGroupJson = service.toFormGroup(FIELDS).value;
      expect(formGroupJson).toEqual(payload);
    }
  );
});
