import {Component, forwardRef, Input, OnInit} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR, SelectControlValueAccessor, SelectMultipleControlValueAccessor} from '@angular/forms';
import {FormFieldModel} from '../form-services/form-models/form-field.model';
import {MultiselectDropdownField} from '../form-services/form-models/multiselect-dropdown-field.model';

// @ts-ignore
@Component({
  selector: 'app-multiselect-dropdown',
  templateUrl: './multiselect-dropdown.component.html',
  styleUrls: ['./multiselect-dropdown.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MultiselectDropdownComponent),
      multi: true
    }
  ]
})
export class MultiselectDropdownComponent implements ControlValueAccessor {
  @Input() field: MultiselectDropdownField;
  value: string;
  onChange: (value: any) => void;
  onTouched: () => void;
  disabled: boolean;
  addItem(key: string) {
    const newItem = this.field.options.find((opt) => opt.key === key);
    this.field.value.push(newItem);
    this.value = null;
  }

  removeItem(index) {
    this.field.value.splice(index, 1);
  }

  registerOnChange(fn: any): void {
    this.onChange = this.addItem;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(inputValue: string): void {
    this.value = inputValue ? inputValue : '';
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

}
