import {FormFieldModel} from '../form-models/form-field.model';
import {Injectable} from '@angular/core';
import {DropdownField} from '../form-models/dropdown-form-field.model';
import {TextField} from '../form-models/text-form-field.model';
import {MultiselectDropdownField} from '../form-models/multiselect-dropdown-field.model';

@Injectable()
export class FormDataService {
  getNewItemFields() {
    const fields: FormFieldModel<any>[] = [
      new TextField({
        key: 'name',
        label: 'Name',
        value: '',
        required: true,
        order: 1
      }),
      new TextField({
        key: 'ggn',
        label: 'GGN',
        value: '',
        order: 2
      }),
      new DropdownField({
        key: 'country',
        label: 'Country',
        options: [
          {key: 'solid',  value: 'Solid'},
          {key: 'great',  value: 'Great'},
          {key: 'good',   value: 'Good'},
          {key: 'unproven', value: 'Unproven'}
        ],
        order: 3
      }),
      new MultiselectDropdownField({
        key: 'roles',
        label: 'Roles',
        options: [
          {key: 'test1',  value: 'Test1'},
          {key: 'test2',  value: 'Test2'}
        ],
        order: 4
      }),
      new MultiselectDropdownField({
        key: 'sector',
        label: 'Sector',
        options: [
          {key: 'test1',  value: 'Test1'},
          {key: 'test2',  value: 'Test2'}
        ],
        order: 5
      })
    ];
    return fields.sort((a, b) => a.order - b.order);
  }
  getFields(id: number) {
    const fields: FormFieldModel<any>[] = [
      new TextField({
        key: 'name',
        label: 'Name',
        value: 'Wonderful Ltd',
        required: true,
        order: 1
      }),
      new TextField({
        key: 'ggn',
        label: 'GGN',
        value: '',
        order: 2
      }),
      new DropdownField({
        key: 'country',
        label: 'Country',
        options: [
          {key: 'solid',  value: 'Solid'},
          {key: 'great',  value: 'Great'},
          {key: 'good',   value: 'Good'},
          {key: 'unproven', value: 'Unproven'}
        ],
        order: 3
      }),
      new MultiselectDropdownField({
        key: 'roles',
        label: 'Roles',
        value: [{key: 'test1', value: 'Test1'}],
        options: [
          {key: 'test1',  value: 'Test1'},
          {key: 'test2',  value: 'Test2'}
        ],
        order: 4
      }),
      new MultiselectDropdownField({
        key: 'sector',
        label: 'Sector',
        value: [{key: 'test1', value: 'Test1'},
          {key: 'test2', value: 'Test2'}],
        options: [
          {key: 'test1',  value: 'Test1'},
          {key: 'test2',  value: 'Test2'}
        ],
        order: 5
      })
    ];

    return fields.sort((a, b) => a.order - b.order);
  }
}
