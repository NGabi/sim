import {FormFieldModel} from './form-field.model';

export class TextField extends FormFieldModel<string> {
  controlType = 'text';
  type: string;

  constructor(options: {} = {}) {
    super(options);
    this.type = options['type'] || '';
  }
}
