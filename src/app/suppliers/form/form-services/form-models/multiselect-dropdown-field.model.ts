import {FormFieldModel} from './form-field.model';

export class MultiselectDropdownField  extends FormFieldModel<{key: string, value: string}[]> {
  controlType = 'multiselectDropdown';
  options: {key: string, value: string}[] = [];

  constructor(options: {} = {}) {
    super(options);
    this.options = options['options'] || [];
    this.value = options['value'] || [];
  }
}
