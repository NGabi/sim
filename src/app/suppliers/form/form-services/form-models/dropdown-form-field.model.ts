import {FormFieldModel} from './form-field.model';

export class DropdownField extends FormFieldModel<string> {
  controlType = 'dropdown';
  options: {key: string, value: string}[] = [];

  constructor(options: {} = {}) {
    super(options);
    this.options = options['options'] || [];
  }
}
