import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {DropdownField} from '../form-services/form-models/dropdown-form-field.model';
import {TextField} from '../form-services/form-models/text-form-field.model';
import {MultiselectDropdownField} from '../form-services/form-models/multiselect-dropdown-field.model';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss']
})
export class DynamicFormComponent {
  @Input() field: TextField & DropdownField & MultiselectDropdownField;
  @Input() form: FormGroup;
  get isValid() { return this.form.controls[this.field.key].valid; }
}
